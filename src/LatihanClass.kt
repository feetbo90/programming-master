

class Unggas(){

    // properti variabel , properti fungsi

    // properti variabel
    var kaki : Int = 0
    var nama : String = ""

    init {
        println("Objek dibuat untuk unggas")
    }

    fun makan(nama : String){
        println("$nama sedang makan")
    }





}


class Mamalia(var nama : String){



    init {
     println("ini dari primary constructor $nama ")
    }

    // secondary constractor
    constructor(nama : String , kaki : Int):this(nama) {

        println("ini dari secondary constructor $nama dan jumlah kaki $kaki")
    }


    fun bernapas():Unit{
        println("$nama sedang bernapas")
    }


    fun makan():Unit{
        println("$nama sedang makan")
    }

    fun minum() :Unit{
        println("$nama sedang minum")
    }







}


// Constructor primary
// dia bisa membuat properti variabel secara langsung
// secondary Constructor



fun main(args: Array<String>) {

    //    Unggas itik = new Unggas();

    var itik = Unggas()
    itik.kaki = 2
    println(itik.kaki)

    itik.makan("Itik")



    var burungCendrawasih = Unggas()
    burungCendrawasih.kaki = 4
    println(burungCendrawasih.kaki)
    burungCendrawasih.makan("Burung Cendrawasih")



    var Sapi = Mamalia("Sapi")

    var kambingSecondaryConst = Mamalia("Kambing" , 4 )

    kambingSecondaryConst.bernapas()
    kambingSecondaryConst.makan()
    kambingSecondaryConst.minum()

    Sapi.bernapas()
    Sapi.minum()
    Sapi.makan()



}








