package pertemuan8

import java.util.Scanner

class Mahasiswa{

    var nama: String = ""
    var nilai: IntArray? = null

    constructor(nama: String, nilai:IntArray){
        this.nama = nama
        this.nilai = nilai
    }

    fun printNilai()
    {
        for(i in 0..nilai!!.size -1)
        {
            print("${nilai!![i]} ")
        }
    }


}


fun main(args: Array<String>) {

    var input = Scanner(System.`in`)

    println("masukkan berapa banyak nilai")
    var n = input.nextInt()


    var kumpulanInt = IntArray(n, {1})
    for(i in 0..n-1)
    {
        println("masukkan index ke ${i}")
        kumpulanInt[i] = input.nextInt()
    }

    // index 0 1 2
    // nilai 70 80 70

    var iqbal = Mahasiswa("Iqbal", kumpulanInt)
    iqbal.printNilai()
}