package pertemuan7


interface Interface1{

    fun contoh1(testing : String):Unit

}

interface Interface2{
    fun contoh2(testing : String)
}


fun contoh(testing : String){
    println("testing")
}


class Contoh1:Interface1, Interface2{
    override fun contoh2(testing: String) {
        println("ini contoh 2 di clas Contoh $testing")

    }

    override fun contoh1(testing: String) {

        println("ini contoh 1 di clas Contoh $testing")
    }

}


fun main(args: Array<String>) {

    var contoh = Contoh1()
    contoh.contoh1("Testing")

}

