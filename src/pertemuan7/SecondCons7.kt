package pertemuan7

// primary constructor
open class Binatang(var warna : String){

    //var warna : String = ""

    fun eat(){
        println("Animal makan ")
    }

    fun warnaKucing()
    {
        println("warnanya adalah : $warna")
    }

    init {
        println("warna di init : $warna")
    }


}



class Kucing(warna:String ,kaki : Int): Binatang(warna)
{
    init {
        println("warna di init $warna dan kaki kucing $kaki")
    }

    


}






fun main(args: Array<String>) {



    var tom = Kucing("Biru", 4)
    tom.eat()
    tom.warnaKucing()

}