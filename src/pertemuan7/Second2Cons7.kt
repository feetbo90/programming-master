package pertemuan7

// primary constructor
open class Binatang2{

    //var warna : String = ""

    fun eat(){
        println("Animal makan ")
    }

    fun warnaKucing()
    {
//        println("warnanya adalah : $warna")
    }

    init {
        //      println("warna di init : $warna")
    }

    // secondary constructor
    constructor(warna :String ){
        println("ini di secondary $warna")
    }


}



class Kucing2: Binatang2
{
    init {
//        println("warna di init $warna dan kaki kucing $kaki")
    }


    //secondary constructor
    constructor(warna: String, kaki: Int):super(warna)
    {
        println("ini di secondari pertemuan7.Kucing $warna dan kaki $kaki")
    }


}






fun main(args: Array<String>) {



    var tom = Kucing2("Biru", 4)
    //tom.eat()
    //tom.warnaKucing()

}