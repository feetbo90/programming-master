package pertemuan7


class Object1{

    fun satu(){

    }

}

object Object2{

    var varObjectDua : String = ""

    fun objectDua()
    {
        println("di objek dua")
    }

}



fun main(args: Array<String>) {

    var object1 = Object1()
    object1.satu()


    Object2.varObjectDua = "Pertama"
    Object2.objectDua()
    println("${Object2.varObjectDua}")

    var ob = Object2
    ob.objectDua()


}