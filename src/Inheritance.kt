


// class induk
open class Animal{

     var kaki : Int = 0
    //protected var warna : String = "Merah"
    final var pi : Double = 3.14

    open fun eat()
    {
        println("Animal Makan")
    }


}

open class Contoh{

    fun eatContoh(){
        println("makan")
    }
}


// class anak

class Cat:Animal(){



    override fun eat(){
        println("Animal diganti menjadi fungsi di kucing")
    }

    fun eatCat()
    {
        println("pertemuan7.Kucing sedang Makan")
    }



}


class Rabbit{

    fun eatRabbit(){
        println("Kelinci sedang Makan")
    }

}



fun main(args: Array<String>) {

    var kucing = Cat()
    kucing.eat()
    kucing.eatCat()
    //kucing.warna = "Merah Baru"


    var kelinci = Rabbit()
    kelinci.eatRabbit()

}