package latihan

import java.util.Scanner

fun main(args: Array<String>) {

    var input = Scanner(System.`in`)

    var bilangan1:Int
    var bilangan2:Int

    println("Berapa banyak pengulangan : ")
    bilangan1 = input.nextInt()


    var nilai :Int = 1
    while (nilai <= bilangan1)
    {
        println(nilai)
        nilai++

    }

    println()
    nilai = 1
    while (bilangan1 >= nilai)
    {
        println(bilangan1)
        bilangan1--
    }


    for (i in 1..10) {

        println("memakai in ${i}")

    }

    for (i in 1 until 10) {
        println("memakai until ${i}")
    }

    for (i in 10 downTo 0) {
        println("memakai down To ${i}")
    }

    for (i in 1..10 step 2) {
        println("memakai step ${i}")
    }

    for (i in 10 downTo 1 step 2) {

        println("memakai downTo step ${i}")
    }

    for (i in 0..10 step 2)
    {
        println("memakai step ${i}")
    }





}