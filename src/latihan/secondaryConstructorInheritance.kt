package latihan


open class Animal(color: String){

    var color: String = ""

    constructor(color:String, breed: String):this(color){
        this.color = color

    }

}


open class Cat{

    var warna :String = ""

    constructor( warna : String){
        this.warna = warna
    }
}

open class Dog(var color2: String, var breed:String):Animal(color2, breed){
    //var breed : String = ""

    //constructor(color: String, breed : String):super("Warna"){
      //  this.breed = breed
    //}

}

open class Test{

}

object VirtualAnimal:Dog("Merah", "hehew"){



}


interface Control{
    fun pindahChannel(channel : Int)
}

interface Control2{
    fun pindahChannel(channel: Int){
        println("Control 2")
    }
}


class Okey:Control, Control2{
    override fun pindahChannel(channel: Int) {
        super<Control2>.pindahChannel(channel)
    }


}





fun main(args: Array<String>) {


    var Putti = Dog("Pink", "test")
    var test = Cat("Merah")
    //Putti.color

    var Key = Okey()
    Key.pindahChannel(1)

}