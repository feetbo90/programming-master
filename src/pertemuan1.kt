

// program utama dari kotlin
fun main(args:Array<String>)
{

    // variabel , type data
    // Int , Double, String, Boolean
    // cara deklarasi variabel adalah
    // val/var nama_variabel:type_data = nilai

    var bilangan1 : Double = 10.0

    val bilangan3 : Int = 12

    //double bilangan1 = 10.0

    bilangan1 = 11.0
    var bilangan2 : Double = 12.0



    // operator aritmatika
    // * , / , - , +, %

    var hasil = bilangan1 + bilangan2

    println("maka hasil penjumlahannya adalah : ${hasil}")





}