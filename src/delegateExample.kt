
interface Canfly{
    fun fly()
}

class Bird(f: Canfly) : Canfly by f

class Wings{
    fun move():Unit{
        println("Fly move")

    }
}

class Bat : Canfly by AnimalWithWings()

class AnimalWithWings:Canfly{
    val wings:Wings = Wings()
    override fun fly() = wings.move()


}





fun main(args: Array<String>) {
    val birdWithWings = Bird(AnimalWithWings())
    birdWithWings.fly()

    val bat = Bat()
    bat.fly()
}