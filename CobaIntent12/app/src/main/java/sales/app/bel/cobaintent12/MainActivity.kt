package sales.app.bel.cobaintent12

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn.setOnClickListener {
            var nama = edit.text.toString()

            // intent cara 1
           /* var intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("nama", nama)
            startActivity(intent)
            // intent cara 2
            //newIntent(this, nama )
*/
            // cara 3
            launchActivity<SecondActivity> {
                putExtra(INTENT_USER_ID, nama)
            }


        }

    }

    fun test3()
    {
        nilai = "Ok3"
    }

    fun test()
    {
        nilai = "Ok2"
    }


    companion object{
        private val INTENT_USER_ID = "nama"

        var nilai:String = "Ok"
        fun newIntent(context: Context, value:String = "Ok"): Intent {
            val intent = Intent(context, SecondActivity::class.java)
            intent.putExtra(INTENT_USER_ID, value)
            return intent
        }
    }

    inline fun <reified T : Any> newIntent(context: Context): Intent =
            Intent(context, T::class.java)

    inline fun <reified T : Any> Activity.launchActivity(
            requestCode: Int = -1,
            options: Bundle? = null,
             init: Intent.() -> Unit ) {

        val intent = newIntent<T>(this)
        intent.init()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent, options)
        } else {
            startActivity(intent)
        }
    }

    fun jumlah(bil1 : Int, bil2: Int):Int{

        return bil1 + bil2
    }


}
