package sales.app.bel.cobaintent12

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

/**
 * Created by root on 16/11/17.
 **/


public class SecondActivity : AppCompatActivity(){

    private val INTENT_USER_ID = "nama"

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        var nama = intent.getStringExtra(INTENT_USER_ID)

        edit.setText(nama)


    }


}
