package sales.app.bel.kotlintraining

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

import kotlinx.android.synthetic.main.login.*

/**
 * Created by root on 08/11/17.
 **/

class KotlinNewExtension : AppCompatActivity() {


    var context: Context? = null
    private val INTENT_USER_ID = "user_id"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        val userId = intent.getStringExtra(INTENT_USER_ID)
        requireNotNull(userId) { "no user_id provided in Intent extras" }

        username.setText(userId)
        context = this
        button.setOnClickListener {
            var username = username.text.toString()
            var pass = password.text.toString()
            Toast.makeText(context, "Usernaem : ${username} , dan Password : ${pass}"
                    , Toast.LENGTH_LONG).show()
        }
    }
}