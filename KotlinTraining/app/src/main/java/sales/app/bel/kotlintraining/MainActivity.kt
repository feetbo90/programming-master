package sales.app.bel.kotlintraining

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    var editUsername: EditText? = null
    var editPassword: EditText? = null
    var context:Context? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        editUsername = findViewById<EditText>(R.id.username) as EditText
        editPassword = findViewById<EditText>(R.id.password) as EditText
        var buttonLogin = findViewById<Button>(R.id.button) as Button
        context = this

        val sum = { bil1 : Int, bil2: Int -> bil1 + bil2}

        buttonLogin.setOnClickListener(View.OnClickListener {
            var username = editUsername?.text.toString()


            var pass = editPassword?.text.toString()
            Toast.makeText(context, "Username : ${username} , dan Password : ${pass}"
            , Toast.LENGTH_LONG).show()

            launchActivity<KotlinNewExtension> {
                putExtra(INTENT_USER_ID, "Ok22")
            }

            //val intent = newIntent<KotlinNewExtension>(this)
                    //MainActivity.newIntent(this)
            //startActivity(intent)



        })

    }


    inline fun operation(op:() -> Unit)
    {
        println("coba sebelum op")
        op()
        println("coba setelah op")
    }



    companion object {

        private val INTENT_USER_ID = "user_id"

        fun newIntent(context: Context, value:String = "Ok"): Intent {
            val intent = Intent(context, KotlinNewExtension::class.java)
            intent.putExtra(INTENT_USER_ID, value)
            return intent
        }




        inline fun <reified T : Any> newIntent(context: Context): Intent =
                Intent(context, T::class.java)


        inline fun <reified T : Any> Activity.launchActivity(
                noinline init: Intent.() -> Unit = {}, ) {
            requestCode: Int = -1,
            options: Bundle? = null

            val intent = newIntent<T>(this)
            intent.init()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                startActivityForResult(intent, requestCode, options)
            } else {
                startActivityForResult(intent, requestCode)
            }
        }

         inline fun <reified T : Any> Context.launchActivity(
                options: Bundle? = null,
                 noinline init: Intent.() -> Unit = {}) {

            val intent = newIntent<T>(this)
            intent.init()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                startActivity(intent, options)
            } else {
                startActivity(intent)
            }
        }

    }
}
